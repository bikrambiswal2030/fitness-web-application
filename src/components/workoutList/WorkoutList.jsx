import React from "react";

import "../../workstyle/workoutList.css";

const WorkoutList = ({ children }) => (
  <div className="workout-list">{children}</div>
);

export default WorkoutList;